import { Link } from "react-router-dom";
import NavBar from "../components/NavBar/NavBar";
import CardsHomepage from "../components/Cards/CardsHomepage";

export default function Homepage(){
    return(
        <div>
            <NavBar></NavBar>
            <CardsHomepage></CardsHomepage>
        </div>
    );
}