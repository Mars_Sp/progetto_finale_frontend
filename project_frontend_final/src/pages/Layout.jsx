//nel layout si importa il componente che vogliamo rendere disponibile a tutte le rotte, tipo la navbar
import NavBar from "../components/NavBar/NavBar";
import { useOutlet } from "react-router-dom"; //useOutlet raccoglie tutte le rotte a cui applichiamo il layout
import Footer from "../components/Footer/Footer";

export default function Layout(){
    const outlet = useOutlet()

    return(
        <>
        <NavBar></NavBar> {/* quindi se metti la navbar qui la rendi visibile ovunque */}
        
        {outlet} {/* raccoglie tutte le rotte figlie */}
        <Footer></Footer>
        </>
    )

}
