import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import "../node_modules/bootstrap/dist/css/bootstrap.css"
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js"


import { createBrowserRouter } from 'react-router-dom'
import { RouterProvider } from 'react-router-dom'
import Homepage from './pages/Homepage.jsx'
import FormLogin from './pages/FormLogin.jsx'
import FormRegistrazione from './pages/FormRegistrazione.jsx'
import Layout from './pages/Layout.jsx'
import ProfiloUtente from './pages/ProfiloUtente.jsx'
// import AuthProvider from './components/Context/AuthContext.jsx'
import CorsiUtente from './pages/CorsiUtente.jsx'
import UserContextProvider from './components/Context/UserContextProvider.jsx'
import ListaUtenti from './components/ListaUtenti/ListaUtenti.jsx'

const router = createBrowserRouter([

  {
    element: (
      <UserContextProvider>
        <Layout /> {/* avvolgo il layout con il provider del contesto */}
      </UserContextProvider>
    ),
    children: [ 
      {
        path: "/",
        children: [
          {
            path: "",
            element: <Homepage></Homepage>,
          },
          
          {
            path: "login",
            element: <FormLogin></FormLogin>,
          },

          {
            path: "userRegister",
            element: <FormRegistrazione></FormRegistrazione>,
          },

          {
            path: "ProfiloUtente",
            element: <ProfiloUtente></ProfiloUtente>,
          },

          {
            path: "corsiUtente",
            element: <CorsiUtente></CorsiUtente>,
          },

          {
            path: "listaUtenti",
            element: <ListaUtenti></ListaUtenti>,
          }



        ],
      },
    ],
  },
]);



ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router = {router}> 
  </RouterProvider>
)
