import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useContext } from "react";

import { UserContext } from "../Context/UserContext";
import './ProfiloUtente.css';

export default function ProfiloUtente(){

  const [userData, setUserData, isLogged, setIsLogged] = useContext(UserContext);
  const [ruoli, setRuoli] = useState([]);


  // useEffect(() => {
  //   async function fetchUser() {
  //     const response = await fetch("http://localhost:8080/api/utente/getUtenti");


  //     if (!response.ok) {
  //         throw new Error('Errore durante il recupero dei dati dell\'utente');
  //     }
  //     const data = await response.json();
  //     const match = data.find((utente) => utente.email === userData.email);

  //     if (match) {
  //       setRuoli(match.ruoli.map((ruolo) => ruolo.tipologia));
  //       console.log("Ruoli:", ruoli);
  //     }
  //   } 
  //   if (isLogged) {
  //     fetchUser();
  //   }
  // }, [isLogged, userData.email]);

  useEffect(() => {
    async function fetchUser() {
        try {
            const token = localStorage.getItem("token");
            const response = await fetch("http://localhost:8080/api/utente/getUtenti", {
                headers: {
                  "Authorization": `Bearer ${token}`
                },
            });
            if (!response.ok) {
                throw new Error('Errore durante il recupero dei dati'); //entra qui, probabilemnte perchè non riesce a recuperare il ruolo utente
            }
            const data = await response.json();
            const match = data.find((utente) => utente.email === userData.email);

            if (match) {
                setRuoli(match.ruoli.map((ruolo) => ruolo.tipologia));
                console.log("Ruoli:", ruoli);
            }
        } catch (error) {
            console.error('Errore durante il recupero dei dati', error);
            alert('Errore durante il recupero dei dati');
        }
    }  if (isLogged) {
        fetchUser();
    }
  }, [isLogged, userData.email]);



    return(
      <div className="container">
      <div className="user-profile">
        <div className="card">
          <img src="/./src/immagini/profilo1.jpg" className="card-img-top" alt="Immagine profilo" />
          <div className="card-body">
            <h1 className="card-title" style={{marginBottom: "10%"}}>Profilo personale</h1>
            <div className="user-info">
              <p><strong>Nome:</strong> {userData.nome}</p>
              <p><strong>Cognome:</strong> {userData.cognome}</p>
              <p><strong>Email:</strong> {userData.email}</p>
              <p><strong>Ruolo:</strong> {ruoli.join(", ")}</p>
            </div>
            {/* {isLogged && isAdmin && <button className="edit-profile-button">Modifica Profilo</button>}  */}
          </div>
        </div>
      </div>
    </div>
    );
}
