import React from "react";
import { useContext } from "react";
import { UserContext } from "../Context/UserContext";
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useState } from "react";
import { useEffect } from "react";

export default function ListaUtenti(){
    // const [userData, setUserData, isLoggedIn, setIsLoggedIn, isAdmin, setIsAdmin] = useContext(UserContext);
    const [utenti, setUtenti] = useState([]); //per gestire lo stato degli utenti
    const [newUserData, setNewUserData] = useState({
        nome: '',
        cognome: '',
        email: '' 
    });

    useEffect(()=>{
        async function fetchUtenti(){
            const response = await fetch("http://localhost:8080/api/utente/getUtenti");
            if(!response.ok){
                console.log("Errore");
            }
            else{
                const data = await response.json();
                const utenti = data.map((utente)=>({ //recupero i dati utente
                    nome: utente.nome,
                    cognome: utente.cognome,
                    email: utente.email,
                    ruolo: utente.ruoli.map((ruolo)=>ruolo.tipologia) 
                }));
                setUtenti(utenti);
            }
        }
        fetchUtenti();
    }, []);

    // Funzione per gestire la modifica dell'utente
    const handleModificaUtente = async (userId) => {
        try {
            const response = await fetch(`http://localhost:8080/api/utente/aggiornaUtente/${userId}`, { //"http://localhost:8080/api/utente/aggiornaUtente"
                method: 'PUT',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + Cookies.get("token"),
                },
                body: JSON.stringify({
                    nome: newUserData.nome,
                    cognome: newUserData.cognome,
                    email: newUserData.email
                })
            });
            if (!response.ok) {
                throw new Error('Errore durante la modifica');
            }
            // aggiorna lo stato degli utenti dopo la modifica
            const updatedUtenti = [...utenti]; // copia dell'array degli utenti
            // aggiorna l'utente modificato nell'array
            const utenteIndex = updatedUtenti.findIndex(utente => utente.id === userId);
            updatedUtenti[utenteIndex] = { ...updatedUtenti[utenteIndex], ...newUserData };
            setUtenti(updatedUtenti); // aggiorna lo stato degli utenti
            console.log("Utente modificato con successo");
        } catch (error) {
            console.error('Errore durante la modifica', error);
        }
    };
    



    return (
    <div>
        <h1 style={{color: "white"}}>Lista utenti registrati:</h1>
        <table className="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nome</th>
            <th scope="col">Cognome</th>
            <th scope="col">Email</th>
            <th scope="col">Ruolo</th>
            <th scope="col" colSpan="2">Azioni</th>
            </tr>
        </thead>
        <tbody>
            {utenti.map((utente, index) => (
            <tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>{utente.nome}</td>
                <td>{utente.cognome}</td>
                <td>{utente.email}</td>
                <td>{utente.ruolo.join(", ")}</td> {/* Unisce i ruoli in una stringa separata da virgola */}
                <td>
                    <button data-bs-toggle="modal" data-bs-target="#modificaUtenteModal" className="btn btn-primary">
                        <img src="/src/immagini/modifica.svg" alt="Icona modifica" />
                    </button>
                </td>
                <td>
                    <button onClick={() => handleEliminaUtente(utente.id)} className="btn btn-danger">
                        <img src="/src/immagini/cestino.svg" alt="Icona cestino" />
                    </button>
                </td>
            </tr>
            ))}
        </tbody>
        </table>
        
        {/* Modale per la modifica dell'utente */}
        <div className="modal fade" id="modificaUtenteModal" tabIndex="-1" aria-labelledby="modificaUtenteModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="modificaUtenteModalLabel">Modifica Utente</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <div className="mb-3">
                            <label htmlFor="nome" className="form-label">Nome:</label>
                            <input type="text" className="form-control" id="nome" value={newUserData.nome} onChange={(e) => setNewUserData({ ...newUserData, nome: e.target.value })} />
                        </div>
                        {/* Input per modificare il cognome */}
                        <div className="mb-3">
                            <label htmlFor="cognome" className="form-label">Cognome:</label>
                            <input type="text" className="form-control" id="cognome" value={newUserData.cognome} onChange={(e) => setNewUserData({ ...newUserData, cognome: e.target.value })} />
                        </div>
                        {/* Input per modificare l'email */}
                        <div className="mb-3">
                            <label htmlFor="email" className="form-label">Email:</label>
                            <input type="email" className="form-control" id="email" value={newUserData.email} onChange={(e) => setNewUserData({ ...newUserData, email: e.target.value })} />
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Chiudi</button>
                        <button type="button" className="btn btn-primary" onClick={() => handleModificaUtente(userId)}>Salva modifiche</button>

                    </div>
                </div>
            </div>
        </div>

    </div>

    );
}

