import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import { UserContext } from '../Context/UserContext';
import Cookies from "../../../node_modules/js-cookie";

export default function NavBar() {
    //uso il contesto per distinguere un utente loggato o meno, vedere se è un admin e rendere la navbar dinamica
    const [userData, setUserData, isLogged, setIsLogged, isAdmin, setIsAdmin] = useContext(UserContext);

    //quando l'utente esce dal sito, rimuovo i relativi cookies
    const logout = ()=>{
        Cookies.remove("nome");
        Cookies.remove("cognome");
        Cookies.remove("email");
        Cookies.remove("ruoli");

        setUserData({});
        setIsLogged(false);
    }


    return (
        <nav className="navbar navbar-expand-lg bg-dark fixed-top">
            <div className="container-fluid" >

            {/* creo un collegamento sul logo del sito che mi permette di tornare alla home */}
            <Link className="navbar-brand" to="/"> 
            <img src="/src/immagini/dragon-solid.svg" alt="Logo" style={{ width: '30px', height: 'auto', fill:'white'}} />
            </Link>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarNav">
                     <ul className="navbar-nav ms-auto " style={{ fontSize: '18px', marginRight: '20px'}}> {/* Con ms-auto metto tutti gli elementi a destra del logo */}
                        <li className="nav-item" style={{marginRight: '20px'}}>
                            <Link className="nav-link active text-white" aria-current="page" to="/">Home</Link>
                        </li>
                        
                        {/* se l'utente è loggato, indipendentemente dal ruolo, può visualizzare il suo profilo */}
                        {isLogged ? (
                            <li className="nav-item" style={{ display: 'flex', alignItems: 'center', marginRight: '20px' }}>
                                <Link className="nav-link text-white" to="/ProfiloUtente">Profilo</Link>
                            </li>) : ("")
                            }

                            {/* Se sono loggato posso visualizzare i corsi*/}
                        {isLogged ? (
                            <li className="nav-item" style={{ display: 'flex', alignItems: 'center', marginRight: '20px' }}>
                                <Link className="nav-link text-white" to="/corsiUtente">Corsi</Link>
                            </li>) : ("")}
                            
                            {/* Se sono admin posso visualizzare la lsita degli utenti */}
                        {isLogged && isAdmin ? (
                                <li className="nav-item" style={{ display: 'flex', alignItems: 'center', marginRight: '20px' }}>
                                <Link  className="nav-link text-white" to="/listaUtenti">Utenti</Link>
                                </li>) : ("")}

                        <li className="nav-item" style={{ display: 'flex', alignItems: 'center', marginRight: '20px' }}>
                            
                            {/* uso un operatore ternario per gestire il login e logout:
                                se sono loggato posso effettuare il logout, altrimenti il login */}
                            {isLogged ? (
                            <>
                            <Link className="nav-link text-white" to="/" onClick={logout}>
                                <img src="/src/immagini/logout.svg" alt="Logout" style={{ width: '30px', height: 'auto', marginRight: '10px', marginBottom: '3px', filter: 'invert(100%)' }} /> Logout
                            </Link>
                            </>
                        ) : (
                            <Link className="nav-link text-white" to="/login">
                                <img src="/src/immagini/login.svg" alt="Login" style={{ width: '30px', height: 'auto', marginRight: '10px', 
                                marginBottom: '3px', filter: 'invert(100%)'}} /> Login
                            </Link>
                            )}
                        </li>
                        
                    </ul>
                </div>

            </div>
        </nav>
    );
}
