import React, { useState } from 'react';
import './FormRegistrazione.css';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { UserContext } from "../Context/UserContext";
import { useContext } from "react";

export default function FormRegister() {
    const [userData, setUserData, isLoggedIn, setIsLoggedIn, isAdmin, setIsAdmin] = useContext(UserContext);
    
    const [formData, setFormData] = useState({
        nome: "",
        cognome: "",
        email: "",
        password: "",
    });

    const navigate = useNavigate();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
        ...prevState,
        [name]: value,
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const response = await fetch("http://localhost:8080/api/utente/registrazione", { //endpoint postman
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            },
            body: JSON.stringify(formData),
        });

        if (response.ok) {
            alert("Registrazione completata con successo.");
            navigate("/login"); // Reindirizza l'utente alla pagina di login, DA MIGLIORARE IL REINDIRIZZAMENTO
        } else {
            alert("Si è verificato un errore durante la registrazione.");
        }
    };

    return (
    
    <div className="login-container">
        <h2>Registrazione</h2>
        <br />
        
            <form onSubmit={handleSubmit}>
            <input type="text" name="nome" className="form-control mb-2" placeholder="Nome" required value={formData.nome} onChange={handleChange} pattern="[a-zA-Zàèìòù]{1,50}" title="Inserire un nome valido."
            />
            <input type="text" name="cognome" className="form-control mb-2" placeholder="Cognome" required value={formData.cognome} onChange={handleChange} 
                pattern="[a-zA-Zàèìòù]{1,50}" title="Inserire un cognome valido." />
            <input type="email" name="email" className="form-control mb-2" placeholder="Email" required value={formData.email} onChange={handleChange}
                pattern="[A-z0-9.+_-]+@[A-z0-9._-]+\.[A-z]{2,8}"
                title="Inserire un indirizzo email valido." 
            />
            <input type="password" name="password" className="form-control mb-2"  placeholder="Password" required value={formData.password} 
                onChange={handleChange} pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,20}" 
                title="La password deve essere lunga 6-20 caratteri e contenere almeno un numero, una lettera maiuscola, una minuscola e un carattere speciale (@$!%*?&)." />

            <button type="submit">Registrati</button>

            </form>
            <div className="return-to" style={{ display: "flex", justifyContent: "space-between" }}>
                <Link to="/" style={{ textDecoration: 'none' }}>Torna alla Home</Link>
                <Link to="/login" style={{ textDecoration: 'none' }}>Vai al login</Link>
            </div>

        
    </div>
    );
}
