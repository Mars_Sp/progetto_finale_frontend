import React from "react";
import { Link } from 'react-router-dom';
import './Cards.css';

//cards dei corsi passati sulla homepage, l'utente non loggato non può visualizzare tutti i dettagli
export default function CardsHomepage({ isAuthenticated }) { //passo isAuthenticated nel caso un utente sia loggato

    const CorsiInformatici = [
        { 
        titolo: "Corso di sviluppo web",
        descrizione: "Impara a creare siti web professionali con HTML, CSS e JavaScript. Acquisisci competenze fondamentali per la realizzazione di progetti web.",
        immagine: "/src/immagini/web_dev.jpg", 
        link: "/corso-sviluppo-web"
        },
    
        { 
        titolo: "Corso di data science",
        descrizione: "Entra nel mondo dell'analisi dei dati. Impara a utilizzare Python e le librerie più diffuse per analizzare dati, creare modelli predittivi e visualizzare i risultati.",
        immagine: "/src/immagini/data science.jpg", 
        link: "/corso-data-science"
        },
    
        { 
        titolo: "Corso Figma",
        descrizione: "Scopri come progettare interfacce utente efficaci e accattivanti con Figma. Impara a creare wireframe, prototipi interattivi e collaborare con il tuo team di sviluppo.",
        immagine: "/src/immagini/ICONA_Figma-1.png", 
        link: "/corso-figma"
        }
    ];

    return (
        <section className="container">
            <h2 style={{ letterSpacing: "5px", fontSize: "40px", color: "white"}}>CORSI INFORMATICI EDIZIONI PASSATE!</h2>
            <p className="paragrafo" style={{ fontSize: "18px", color: "white"}}>
            Scopri i nostri corsi informativi e avvia il tuo percorso di apprendimento nel mondo della tecnologia!
            </p>

            <div className="display-section">
                {CorsiInformatici.map((corso, index) => (
                    <div className="card" key={index}>
                        <h3>{corso.titolo}</h3>
                        <img className="card-img-top" src={corso.immagine} alt={corso.titolo} />
                        <div className="card-body">
                            <p className="card-text">{corso.descrizione}</p>
                            {isAuthenticated ? (
                                <Link to={corso.link} className="btn btn-primary">Scopri di più</Link>
                            ) : (
                                <p style={{fontStyle: 'italic'}}>Per avere più informazioni sui corsi, effettua il <Link to="/login">login</Link> o <Link to="/userRegister">registrazione</Link>.</p>
                            )}
                        </div>
                    </div>
                ))}
            </div>
        </section>
    );
}
