import { Link } from "react-router-dom";
import { useState, useContext } from "react";
import { UserContext } from "../Context/UserContext";
import { useNavigate } from "react-router-dom";
import './FormLogin.css';

import Cookies from "../../../node_modules/js-cookie";
import { jwtDecode } from "../../../node_modules/jwt-decode";

export default function FormLogin() {
    const [userData, setUserData, isLogged, setIsLogged, isAdmin, setIsAdmin] = useContext(UserContext);

    //Dati del form
    const [formData, setFormData] = useState({
        email: "",
        password: ""
    });

    //Per navigare tra le pagine
    const navigate = useNavigate();

    //Gestione dati
    function handleChange(e){
        const {name, value} = e.target;
        setFormData({...formData, [name]: value});
    };


    //FETCH LOGIN
    //@POST
    const handleSubmit = async (e) => {
        e.preventDefault();
    
        try {
            const response = await fetch("http://localhost:8080/api/utente/login", {
                method: "POST",
                headers: { 
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(formData)
            });
    
            if (response.ok) {
                const tokenResponse = await response.json();
                const token = tokenResponse.token;
                const decodToken = jwtDecode(token); //decodifico il token con jwtDecode
    
                //Recupero i dati
                const nome = decodToken.nome;
                const cognome = decodToken.cognome;
                const email = decodToken.email;
                const ruoli = decodToken.ruoli;
    
                //Setto i dati
                setUserData({ nome, cognome, email });
    
                //Li memorizzo nei Cookies
                Cookies.set("nome", nome, { expires: 31 });
                Cookies.set("cognome", cognome, { expires: 31 });
                Cookies.set("email", email, { expires: 31 });
                Cookies.set("ruoli", ruoli, { expires: 31 });

                Cookies.set("token", token, {expires : 31}); //salvo il token
    
                setIsLogged(true);
                setIsAdmin(ruoli.includes("Admin")); //Imposta isAdmin in base al ruolo Admin
                alert("Login riuscito.");
                //Dopo aver effettuato la login si apre la pagina del profilo utente
                navigate("/ProfiloUtente");
            } else {
                alert("Errore durante l'accesso.");
            }
        } catch (error) {
            console.error("Errore durante il login:", error);
            alert("Errore di connessione al server");
        }
    }
    


return (
    <div className="login-container">
    <h2>Login</h2>

    <form onSubmit={handleSubmit}>
        <input type="email" className="form-control" name="email" placeholder="Email" required value={formData.email} onChange={handleChange} />
        <input type="password" className="form-control" name="password" placeholder="Password" required value={formData.password} onChange={handleChange}/>

        {/* Bottone login */}
        <button type="submit">Login</button>
    </form>

    <div className="remember-me">
        <label htmlFor="remember">Ricordami</label>
        <input type="checkbox" name="remember" id="remember" />
    </div>

    {/* se l'utente dimentica la password */}
    <div className="forgot-password">
        <a href="#" style={{textDecoration: 'none'}}>Hai dimenticato la password?</a> 
    </div>

    <div className="alt-login">
        <a href="#"><i className="fab fa-google" style={{ color: '#DB4437' }}></i></a>
        <a href="#"><i className="fab fa-twitter" style={{ color: '#3498db' }}></i></a>
        <a href="#"><i className="fab fa-facebook" style={{ color: '#3b5998' }}></i></a>
        <a href="#"><i className="fab fa-apple" style={{ color: 'black' }}></i></a>
    </div>
    
    {/* Testo di suggerimento per la registrazione */}
    <p style={{ margin:'20px', textAlign: 'center'}}>Non hai un account? 
        <Link to="/userRegister" style={{ textDecoration: 'none' }}>Registrati qui</Link>
    </p>

    {/* Link per tornare alla homepage */}
    <div style={{ display:"flex", justifyContent: "start", alignItems:"center"}}>
        <Link to="/" className="homepage-link" style={{textDecoration: 'none'}}>Torna alla Home</Link>
    </div>

    </div>
    );
}
