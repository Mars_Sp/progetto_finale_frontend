import React from "react";
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useState } from "react";
import { useEffect } from "react";
import './CorsiUtente.css'
import { UserContext } from "../Context/UserContext";
import { useContext } from "react";
import Cookies from "js-cookie";

export default function CorsiUtente(){
    const [userData, setUserData, isLoggedIn, setIsLoggedIn, isAdmin, setIsAdmin] = useContext(UserContext);

    const [userCourses, setUserCourses] = useState([]);
    const [allCourses, setAllCourses] = useState([]);
    const [docenti, setDocenti] = useState([]);
    const categorie = ['Frontend', 'Backend', 'Ingegneria del software'];

    // const [newCourseId, setNewCourseId] = useState('');
    const [newCourseData, setNewCourseData] = useState({ //inizialmente vuoto
        nome_corso: "",
        descrizione_breve: "",
        descrizione_completa: "",
        durata: 0,
        categoria: "",
        // categoria: {
        //     id_ca: 0,
        //     nome_categoria: ""
        // },
        id_doc: 0
    });


    //simulo il recupero dati dal backend perchè manca l'endpoint che mi permette di recuperare i dati dei corsi 
    useEffect(() => {
        const fetchUserCourses = async () => {
            const userData = [
                { id: 1, title: 'Corso di HTML' },
                { id: 2, title: 'Corso di CSS' },
            ];
            setUserCourses(userData); // aggiorniamo lo stato con i corsi dell'utente
        };

        const fetchAllCourses = async () => {
            // esempio di dati di tutti i corsi disponibili
            const allCoursesData = [
                { id: 1, title: 'Corso di HTML' },
                { id: 2, title: 'Corso di CSS' },
                { id: 3, title: 'Corso di JavaScript' },
                // { id: 4, title: 'Corso di React' },
            ];
            setAllCourses(allCoursesData); // Aggiorniamo lo stato con tutti i corsi disponibili
        };
        fetchUserCourses();
        fetchAllCourses();
    }, []);


    //FUNZIONALITA' PER L'UTENTE NON ADMIN
    // iscrizione a un corso
    const handleEnroll = (course) => {
        setUserCourses([...userCourses, course]);
    };

    // disiscrizione da un corso
    const handleUnenroll = (courseId) => {
        setUserCourses(userCourses.filter(course => course.id !== courseId));
    };



    //per ottenere la lista degli utenti che sono dei docenti (con id = 2)
    useEffect(() =>{
        async function fetchDocenti(){
            const response = await fetch("http://localhost:8080/api/utente/getUtenti?id_r=2"); 
            if (response.ok) {
                const data = await response.json();
                setDocenti(data);
            } else {
                console.log("Errore nel recupero dei dati del docente");
            }
        }
        fetchDocenti();
    }, []);

    // per gestire l'aggiunta di un nuovo corso
    const handleCreateCourse = async () => {
        try {

            const response = await fetch("http://localhost:8080/api/corso", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    // "Authorization": `Bearer ${token}` //riprendo il token per l'autorizzazione
                    Authorization: "Bearer " + Cookies.get("token"),
                },
                body: JSON.stringify(newCourseData)
            });
            //console.log("Cookie corso: " + Cookies.get("token"));

            if (response.ok) {
                // Aggiorna lo stato dei corsi con il nuovo corso aggiunto
                setAllCourses([...allCourses, newCourseData]);
                alert("Corso aggiunto con successo!");
            } else {
                alert("Si è verificato un errore durante l'aggiunta del corso.");
            }
            } catch (error) {
            console.error("Errore durante la richiesta di aggiunta del corso:", error);
            alert("Si è verificato un errore durante la richiesta di aggiunta del corso.");
            }
        };



    return(
        <div className="container-corsi" style={{textDecoration: "none"}}>
            {/* Iscrizioni ai corsi indipendentemente dal ruolo dell'utente: */}
            <h4>Corsi a cui sei iscritto:</h4>
            <ul>
                {userCourses.map(course => (
                    <li key={course.id} style={{display: "flex", alignItems: "center", marginBottom: "10px"}}>
                        {/* Link alla pagina del corso */}
                        <Link to={`/corsiUtente/${course.id}`} className="corso-link">{course.title}</Link>
                        <button onClick={() => handleUnenroll(course.id)} className="button-disiscriviti">Disiscriviti</button>
                    </li>
                ))}
            </ul>

            <h4>Tutti i Corsi Disponibili</h4>
            <ul>
                {allCourses.map(course => (
                    <li key={course.id} style={{display: "flex", alignItems: "center", marginBottom: "10px"}}>
                        {/* Link alla pagina SPECIFICA del corso, ma non reindirizza a niente perchè non c'è una pagina descrizione per il corso */}
                        <Link to={`/corsiUtente/${course.id}`} className="corso-link">{course.title}</Link>
                        {userCourses.some(userCourse => userCourse.id === course.id) ? (
                            <button onClick={() => handleUnenroll(course.id)} className="button-disiscriviti">Disiscriviti</button>
                        ) : (
                            <button onClick={() => handleEnroll(course)} className="button-iscriviti">Iscriviti</button>
                        )}
                    </li>
                ))}
            </ul>

            {/* Funzionalità per l'admin */}
            {isAdmin && (
    <div>
        <button type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" className="button-admin">Aggiungi Corso</button>
        
        <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">Aggiungi un nuovo corso</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <label>Nome del corso:</label>
                        <input type="text" className="new-course-form" placeholder="Nome del corso" value={newCourseData.nome_corso} onChange={(e) => setNewCourseData({ ...newCourseData, nome_corso: e.target.value })} />

                        <label>Descrizione breve:</label>
                        <input type="text" className="new-course-form" placeholder="Descrizione breve" value={newCourseData.descrizione_breve} onChange={(e) => setNewCourseData({ ...newCourseData, descrizione_breve: e.target.value })} />

                        <label>Descrizione completa:</label>
                        <input type="text" className="new-course-form" placeholder="Descrizione completa" value={newCourseData.descrizione_completa} onChange={(e) => setNewCourseData({ ...newCourseData, descrizione_completa: e.target.value })} />

                        <label>Durata:</label>
                        <input type="number" className="new-course-form" value={newCourseData.durata} onChange={(e) => setNewCourseData({ ...newCourseData, durata: parseInt(e.target.value) })} />

                        <label>Categoria:</label>
                        <div>
                            <select value={newCourseData.categoria} onChange={(e) => setNewCourseData({ ...newCourseData, categoria: e.target.value })}>
                                <option value="">Seleziona una categoria</option>
                                {categorie.map(categoria => (
                                    <option key={categoria} value={categoria}>{categoria}</option>
                                ))}
                            </select>
                        </div>
                        
                        <label>Seleziona un docente:</label>
                        <div>
                            <select name="id_doc" className="new-course-form" value={newCourseData.id_doc} onChange={(e) => setNewCourseData({ ...newCourseData, id_doc: parseInt(e.target.value) })}>
                                <option value="">Seleziona un docente</option>
                                {docenti.map((docente) => (
                                    <option key={docente.id} value={docente.id}>
                                        {docente.nome} {docente.cognome}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Chiudi</button>
                        <button onClick={handleCreateCourse} className="btn btn-primary">Aggiungi Corso</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
)}


        </div>
    );
}