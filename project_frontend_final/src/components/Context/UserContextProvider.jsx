import { UserContext } from "../Context/UserContext"
import { useState } from "react";


export default function UserContextProvider({ children }) {
    const [userData, setUserData] = useState({
        nome: "",
        cognome: "",
        email: "",
        ruoli: [""],
        });
        
        //Tengo traccia dell'utente loggato
        const [isLogged, setIsLogged] = useState(false);
        
        //Tengo traccia dell'utente admin
        const [isAdmin, setIsAdmin] = useState(false);
        
        return (
            <UserContext.Provider
            value={[userData, setUserData, isLogged, setIsLogged, isAdmin, setIsAdmin]} //Rendo globali questi dati
            >
            {children}
            </UserContext.Provider>
        );
        }
        
