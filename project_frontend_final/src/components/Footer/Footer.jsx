import React from "react";
import './Footer.css';

export default function Footer() {
    return (
        <footer className="fixed-bottom bg-dark">
            <div className="container-footer">
                <div className="row">
                    <div className="col-md-6" style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'start', paddingLeft: '15px', marginTop: '25px', fontSize: '14px'}}>
                        &copy; 2024 Copyright
                    </div>
                    <div className="col-md-6 text-right" style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                        <div className="contact-info" style={{ color: "white", fontSize: "14px" }}>
                            Contatti: <br />
                            nostraAzienda@gmail.com
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
}
