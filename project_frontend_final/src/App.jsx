import { useState } from 'react'
import './App.css'

import FormLogin from './components/FormLogin/FormLogin'
import FormRegistrazione from './components/FormRegistrazione/FormRegistrazione'
import NavBar from './components/NavBar/NavBar'
import Footer from './components/Footer/Footer'
import CardsHomepage from './components/Cards/CardsHomepage'

import CorsiUtente from './components/CorsiUtente/CorsiUtente'
import ProfiloUtente from './components/ProfiloUtente/ProfiloUtente'
import ListaUtenti from './components/ListaUtenti/ListaUtenti'

function App() {
  // const {isLogged} = useAuth();

  return (
    <>
      <FormLogin></FormLogin>
      <FormRegistrazione></FormRegistrazione>
      <Homepage></Homepage>
      <NavBar></NavBar>
      <Footer></Footer>
      <CardsHomepage></CardsHomepage>
      <ProfiloUtente></ProfiloUtente>
      <CorsiUtente></CorsiUtente>
      <ListaUtenti></ListaUtenti>
      


    </>
  )
}

export default App
